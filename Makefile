init:
	+@ echo

build:
	+@ docker-compose build

dev:
	+@ docker-compose up

shell:
	+@ docker-compose exec proxy sh
